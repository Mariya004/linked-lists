#include"LinkedList.cpp"
int main(){
	LinkedList<int> ll;
	cout<<"Creating Linked List"<<endl;
	ll.create_LL();
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Inserting values to beginning"<<endl;
	cout<<"Enter the value:";
	int a;
	cin>>a;
	ll.insert_at_beginning(a);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Inserting at end"<<endl;
	cout<<"Enter the value:";
	int b;
	cin>>b;
	ll.insert_at_end(b);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Insert before key"<<endl;
	cout<<"Enter the key:";
	int z;
	cin>>z;
	cout<<"Enter the value:";
	int c;
	cin>>c;
	ll.insert_before_key(z,c);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Insert after key"<<endl;
	cout<<"Enter the key:";
	int y;
	cin>>y;
	cout<<"Enter the value:";
	int d;
	cin>>d;
	ll.insert_after_key(y,d);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Delete from beginning"<<endl;
	ll.delete_from_front();
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Delete from end"<<endl;
	ll.delete_from_end();
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Delete a key"<<endl;
	cout<<"Enter the key:";
	int e;
	cin>>e;
	ll.delete_a_key(e);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Concatenate"<<endl;
	LinkedList<int> ll2;
	cout<<"Enter the second list to concatenate"<<endl;
	ll2.create_LL();
	ll.concatenate(ll2);
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Enter the element to search for:";
	int f;
	cin>>f;
	int pos=ll.search(f);
	if (pos==0){
		cout<<"Element not present"<<endl;
	}
	else{
		cout<<"Element found at position "<<pos<<endl;
	}
	cout<<"Reverse"<<endl;
	ll.reverse();
	cout<<"The list is:";
	ll.display();
	cout<<endl;
	cout<<"Reverse Traversal"<<endl;
	ll.reverse_traversal();
	cout<<endl;
	/*cout<<"Sorting"<<endl;
	ll.sort();
	ll.display();*/
	
}
