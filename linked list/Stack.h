#include<iostream>
using namespace std;
template <class T>
class Stack{
	T S[100];
	int top;
	int size;
	public:
		Stack();
		bool is_empty();
		bool is_full();
		void push(T);
		void pop();
		void display();
		T peek();
		float status();
};
