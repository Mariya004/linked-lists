#include"Node.h"
template <class T>
Node<T>::Node(T data){
	this->data=data;
	this->link=NULL;
}
template <class T>
void Node<T>::set_data(T key){
	this->data=data;

}
template <class T>
T Node<T>::get_data(){
	return data;
}
template <class T>
void Node<T>::set_link(Node* link){
	this->link=link;
}
template <class T>
Node<T>* Node<T>::get_link(){
	return link;
}
template <class T>
LinkedList<T>::LinkedList(){
	head=NULL;
}
template <class T>
void LinkedList<T>::create_LL(){
	string op;
	T x;
	Node<T>* current;
	current=head;
	do{cout<<"Enter the value:";
		cin>>x;
		Node<T>* node=new Node<T>(x);
    	if (head==NULL){
    		head=node;
    		current=node;
		}
		else{
			current->set_link(node);
			current=node;
		}
		cout<<"Add value(y/n)?:";
		cin>>op;
		}while (op=="y");	
}
template <class T>
void LinkedList<T>::display(){
	Node<T>* p=head;
	while (p!=NULL){
		cout<<p->get_data()<<" ";
		p=p->get_link();
	}
}
template <class T>
void LinkedList<T>::insert_at_beginning(T key){
	Node<T>* node=new Node<T> (key);
	node->set_link(head);
	head=node;
}
template <class T>
void LinkedList<T>::insert_at_end(T key){
	Node<T>* p=head;
	while(p->get_link()!=NULL){
		p=p->get_link();
	}
	Node<T>* node=new Node<T>(key);
	node->set_link(NULL);
    p->set_link(node);
}
template <class T>
void LinkedList <T>::insert_after_key(T key,T item){
	Node<T>* p=head;
	while (p->get_data()!=key and p->get_link()!=NULL){
		p=p->get_link();
	}
	Node<T>* node=new Node<T>(item);
	Node<T>* q=p->get_link();
	p->set_link(node);
	node->set_link(q);
}
template <class T>
void LinkedList<T>::insert_before_key(T key,T item){
	Node<T>* p=head;
	Node <T>* q=p->get_link();
	if  (head->get_data()==key){
		Node<T>* node=new Node<T>(item);
		node->set_link(head);
		head=node;	
	}
	
	
	while(q->get_link()!=NULL and q->get_data()!=key){
			p=q;
			q=q->get_link();
		}

	if(q->get_link()==NULL){
		cout<<"Element not found"<<endl;
	}
	else{
		Node<T>* node=new Node<T>(item);
		p->set_link(node);
		node->set_link(q);
}
}
template <class T>
void LinkedList<T>::delete_from_front(){
	Node<T>* p=head;
	head=head->get_link();
	delete p;
	  
}
template <class T>
void LinkedList<T>::delete_from_end(){
	Node<T>* p=head;
	Node<T>* q=p->get_link();
	while(q->get_link()!=NULL){
		p=q;
		q=q->get_link();
	}
	p->set_link(NULL);
	delete(q);
}
template <class T>
void LinkedList<T>::delete_a_key(T key){
	Node<T>* p=head;
	Node<T>*q=p->get_link();
	while(q->get_link()!=NULL and q->get_data()!=key){
		p=q;
		q=q->get_link();
	}
	p->set_link(q->get_link());
	delete(q);
}
template <class T>
void LinkedList<T>::concatenate(LinkedList<T>& ll){
	Node<T>* p=this->head;
	while (p->get_link()!=NULL){
		p=p->get_link();
	}
	p->set_link(ll.head);
}
template <class T>
int LinkedList<T>::search(T key){
	Node<T>* p=head;
	int pos=0;
	while(p->get_link()!=NULL and p->get_data()!=key){
		p=p->get_link();
		pos=pos+1;
	}
	return pos;
}
template <class T>
void LinkedList<T>::reverse(){
	Node<T>*p=NULL;
	Node<T>* q=head;
	Node<T>* r=NULL;
	while(q!=NULL){
		r=q->get_link();
		q->set_link(p);
		p=q;
		q=r;
		
	}
	head=p;
}
template <class T>
void LinkedList<T>::reverse_traversal(Node<T>* p){
    if (p==NULL){
        return; 
    }
    reverse_traversal(p->get_link());
    cout <<p->get_data() << " ";
}
template <class T>
void LinkedList<T>::reverse_traversal(){
    reverse_traversal(head);
}
template<class T>
void LinkedList<T>::sort() {
    Node<T>* p = head;
    while (p!=NULL) {
        Node<T>* q=p->get_link();
        while (q!=NULL){
            if (p->get_data()> q->get_data()) {
                T temp =p->get_data();
                p->set_data(q->get_data());
                q->set_data(temp);
            }

            q=q->get_link();
        }
        p=p->get_link();
    }
}


