#include<iostream>
using namespace std;
template <class T>
class Node{
	private:
		T data;
		Node *link;
	public:
		Node(T);
		void set_data(T);
		T get_data();
		void set_link(Node*);
		Node* get_link();
};
template <class T>
class LinkedList{
    public:
		Node<T>* head;
	public:
		LinkedList();
		void create_LL();
		void display();
		void insert_at_beginning(T);
		void insert_at_end(T);
		void insert_after_key(T,T);
		void insert_before_key(T,T);
		void delete_from_front();
		void delete_from_end();
		void delete_a_key(T);
		void concatenate(LinkedList<T>&);
		int search(T );
		void reverse();
		void reverse_traversal(Node<T>*);
		void reverse_traversal();
		void sort();
};
