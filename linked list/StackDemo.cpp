#include "Stack.cpp"
int main(){
	Stack<int> st;
	st.is_empty();
	cout<<"Enter the number of values to insert:";
	int n;
	cin>>n;
	for(int i=0;i<n;i++){
		cout<<"Do you want to pop(y/n):";
		char ch;
		cin>>ch;
		if(ch=='y'){
			st.pop();
		}
		cout<<"Enter the value:";
		int m;
		cin>>m;
		st.push(m);	
	}
	cout<<"Stack is:";
	st.display();
	cout<<endl;
	cout<<"Do you want to pop last element(y/n):";
	char ch;
	cin>>ch;
	if(ch=='y'){
		st.pop();
	}
	cout<<"Stack is:";
	st.display();
	cout<<endl;
	cout<<"Topmost element:"<<st.peek()<<endl;
	cout<<"Status:"<<st.status()<<"%";
}
